package com.example.steamgiftmobile.http.callbacks;

//callback to handle a generic post request
public interface PostCallback {
    void onSuccess(String response);
    void onError();
}