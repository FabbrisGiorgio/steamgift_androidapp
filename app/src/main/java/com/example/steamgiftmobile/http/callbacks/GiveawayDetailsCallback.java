package com.example.steamgiftmobile.http.callbacks;

import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;
//callback interface to handle the parsing of webpage to extract giveaway details
public interface GiveawayDetailsCallback {
    void onSuccess(GiveawayExtended giveaway);
    void onError(Giveaway giveaway);
}
