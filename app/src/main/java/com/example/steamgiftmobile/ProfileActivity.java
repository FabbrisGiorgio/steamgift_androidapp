package com.example.steamgiftmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.steamgiftmobile.classes.User.GenericUserExtended;
import com.example.steamgiftmobile.http.HTTPClient;
import com.example.steamgiftmobile.http.callbacks.UserProfileCallback;
import com.google.android.material.snackbar.Snackbar;

//Activity to show the users profile
//Actually it's used to show only the personal profile
public class ProfileActivity extends AppCompatActivity {

    private TextView usernameTextView;
    private TextView levelTextView;
    private TextView roleTextView;
    private TextView pointsTextView;
    private TextView enteredTextView;
    private TextView wonTextView;
    private TextView sentTextView;
    private TextView lastOnlineTextView;
    private TextView registeredTextView;
    private TextView commentsTextView;
    private ImageView avatarImage;
    private CardView cardPoints;
    private ProgressBar progressBar;
    private CardView general;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        usernameTextView = findViewById(R.id.textUsername);
        levelTextView = findViewById(R.id.textLevel);
        roleTextView = findViewById(R.id.textRole);
        pointsTextView = findViewById(R.id.textPoints);
        enteredTextView = findViewById(R.id.textGiveawayEntered);
        wonTextView = findViewById(R.id.textGiveawayWon);
        sentTextView = findViewById(R.id.textGiveawayCreated);
        lastOnlineTextView = findViewById(R.id.textLastOnline);
        registeredTextView = findViewById(R.id.textRegistrationDate);
        commentsTextView = findViewById(R.id.textComments);
        avatarImage = findViewById(R.id.avatarImage);
        cardPoints = findViewById(R.id.card_points);
        progressBar = findViewById(R.id.progressBarProfile);
        general = findViewById(R.id.card_general);

        showData(this);
    }

    private void showData(Context context){
        HTTPClient.INSTANCE.loadGenericUserProfile(MainActivity.currentUser.getUsername(), new UserProfileCallback() {
            @Override
            public void onSuccess(GenericUserExtended user) {

                runOnUiThread(() -> {
                    //If we are showing the current user we also show the points
                    if(user.getUsername().equals(MainActivity.currentUser.getUsername())){
                        cardPoints.setVisibility(View.VISIBLE);
                        pointsTextView.setText(String.valueOf(MainActivity.currentUser.getPoints()));
                    }

                    usernameTextView.setText(user.getUsername());
                    levelTextView.setText(String.valueOf(user.getLevel()));
                    roleTextView.setText(user.getRole());
                    enteredTextView.setText(String.valueOf(user.getnEntered()));
                    wonTextView.setText(String.valueOf(user.getnGiftsWon()));
                    sentTextView.setText(String.valueOf(user.getnGiftsSent()));

                    if(user.getLastOnline().equals("Online Now")){
                        lastOnlineTextView.setText("Online");
                    }else{
                        lastOnlineTextView.setText("Offline");
                    }

                    registeredTextView.setText(user.getRegisteredSince());
                    commentsTextView.setText(String.valueOf(user.getnComments()));

                    Glide.with(context)
                            .load(user.getAvatarURL())
                            .apply(RequestOptions.circleCropTransform())
                            .into(avatarImage);

                    progressBar.setVisibility(View.GONE);
                    avatarImage.setVisibility(View.VISIBLE);
                    general.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void onError() {
                runOnUiThread(() -> showErrorSnackbar());
            }
        });
    }

    private void showErrorSnackbar() {
        View rootView = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView,
                "Errore durante il caricamento del profilo", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Riprova", v -> {
            showData(this);
            snackbar.dismiss();
        });
        snackbar.show();
    }
}