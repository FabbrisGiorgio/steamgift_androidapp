package com.example.steamgiftmobile.classes;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;

//NOT IMPLEMENTED YET
public class Comment {

    private String author;
    private long timestamp;
    private String content;
    private String avatarImageURL;

    public Comment(String author,long timestamp,String content,String avatarImageURL){

        setAuthor(author);
        setTimestamp(timestamp);
        setContent(content);
        setAvatarImageURL(avatarImageURL);
    }

    public String getAuthor() {
        return author;
    }

    private void setAuthor(String author) {
        if(author.isEmpty()) throw new IllegalArgumentException("Empty string");
        this.author = author;
    }

    public long getTimestamp() {
        return timestamp;
    }

    private void setTimestamp(long timestamp) {
        if(timestamp <= 0) throw new IllegalArgumentException("Timestamp must be > 0");
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        if(author.isEmpty()) throw new IllegalArgumentException("Empty string");
        this.content = content;
    }

    public String getAvatarImageURL() {
        return avatarImageURL;
    }

    private void setAvatarImageURL(String avatarImageURL) {
        this.avatarImageURL = avatarImageURL;
    }

    // Get comment age by converting the timestamp and comparing to local time
    public String getCommentAge(){

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime datePosted = LocalDateTime.ofInstant(Instant.ofEpochSecond(this.timestamp),
                                                            TimeZone.getDefault().toZoneId());

        long diff = ChronoUnit.SECONDS.between(datePosted,now);

        if(diff < 60){
            return String.valueOf(diff) + " secondi";
        }
        else if(diff < 3600){
            return String.valueOf(diff/60) + " minuti";
        }
        else if (diff < 86400){
            return String.valueOf(diff/3600) + " ore";
        }
        else{
            return String.valueOf(diff/86400) + " giorni";
        }
    }

    @Override
    public String toString() {
        return "Comment{" +
                "author='" + author + '\'' +
                ", timestamp=" + timestamp +
                ", content='" + content + '\'' +
                ", avatarImageURL='" + avatarImageURL + '\'' +
                '}';
    }
}
