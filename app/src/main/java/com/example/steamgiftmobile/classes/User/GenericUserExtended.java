package com.example.steamgiftmobile.classes.User;

import java.util.Objects;

//Extended version of generic user with additional informations
public class GenericUserExtended extends GenericUser{

    private String role;
    private String lastOnline;
    private String registeredSince;
    private int nComments;
    private int nEntered;
    private int nGiftsWon;
    private int nGiftsSent;

    public GenericUserExtended(String username,
                               int level,
                               String avatarURL,
                               String id,
                               String role,
                               String lastOnline,
                               String registeredSince,
                               int nComments,
                               int nEntered,
                               int nGiftsSent,
                               int nGiftsWon){

        super(username,level,avatarURL,id);

        setLastOnline(lastOnline);
        setRole(role);
        setRegisteredSince(registeredSince);
        setnComments(nComments);
        setnEntered(nEntered);
        setnGiftsSent(nGiftsSent);
        setnGiftsWon(nGiftsWon);

    }

    public GenericUserExtended(GenericUser user,String role,
                               String lastOnline,
                               String registeredSince,
                               int nComments,
                               int nEntered,
                               int nGiftsSent,
                               int nGiftsWon){

        super(user.getUsername(), user.getLevel(), user.getAvatarURL(),user.getId());

        setLastOnline(lastOnline);
        setRole(role);
        setRegisteredSince(registeredSince);
        setnComments(nComments);
        setnEntered(nEntered);
        setnGiftsSent(nGiftsSent);
        setnGiftsWon(nGiftsWon);
    }

    public String getRole() {
        return role;
    }

    private void setRole(String role) {
        this.role = role;
    }

    public String getLastOnline() {
        return lastOnline;
    }

    private void setLastOnline(String lastOnline) {
        this.lastOnline = lastOnline;
    }

    public String getRegisteredSince() {
        return registeredSince;
    }

    private void setRegisteredSince(String registeredSince) {
        this.registeredSince = registeredSince;
    }

    public int getnComments() {
        return nComments;
    }

    private void setnComments(int nComments) {
        this.nComments = nComments;
    }

    public int getnEntered() {
        return nEntered;
    }

    private void setnEntered(int nEntered) {
        this.nEntered = nEntered;
    }

    public int getnGiftsWon() {
        return nGiftsWon;
    }

    private void setnGiftsWon(int nGiftsWon) {
        this.nGiftsWon = nGiftsWon;
    }

    public int getnGiftsSent() {
        return nGiftsSent;
    }

    private void setnGiftsSent(int nGiftsSent) {
        this.nGiftsSent = nGiftsSent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GenericUserExtended that = (GenericUserExtended) o;
        return getnComments() == that.getnComments() &&
                getnEntered() == that.getnEntered() &&
                getnGiftsWon() == that.getnGiftsWon() &&
                getnGiftsSent() == that.getnGiftsSent() &&
                getRole().equals(that.getRole()) &&
                getLastOnline().equals(that.getLastOnline()) &&
                getRegisteredSince().equals(that.getRegisteredSince());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getRole(), getLastOnline(), getRegisteredSince(), getnComments(), getnEntered(), getnGiftsWon(), getnGiftsSent());
    }

    @Override
    public String toString() {
        return "GenericUserExtended{" +
                "role='" + role + '\'' +
                ", lastOnline='" + lastOnline + '\'' +
                ", registeredSince='" + registeredSince + '\'' +
                ", nComments=" + nComments +
                ", nEntered=" + nEntered +
                ", nGiftsWon=" + nGiftsWon +
                ", nGiftsSent=" + nGiftsSent +
                '}';
    }
}
