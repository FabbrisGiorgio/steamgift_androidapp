package com.example.steamgiftmobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.steamgiftmobile.adapter.DividerItemDecoration;
import com.example.steamgiftmobile.adapter.MainAdapter;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.Session;
import com.example.steamgiftmobile.classes.User.SteamUser;
import com.example.steamgiftmobile.http.callbacks.GiveawayListCallback;
import com.example.steamgiftmobile.http.HTTPClient;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

// Homepage, activity with a list of all the giveaways currently available
public class MainActivity extends AppCompatActivity {

    public static SteamUser currentUser = new SteamUser();
    private boolean loadedSession = false;
    private Session session;
    private List<Giveaway> currentGiveAwayList;
    private int pageCount;
    private int lastPage;
    private MainAdapter adapter;
    private SwipeRefreshLayout refreshLayout;
    private NavigationView drawerView;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private ImageView userImage;
    private TextView userNameTextView;
    private TextView pointsTextView;
    private TextView levelTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Retrieving session
        session = new Session(this);
        String sessionID = session.getString("sessionID");

        //Check if the user has already been logged
        if (sessionID != null && !sessionID.isEmpty() && !loadedSession){
            currentUser.setSessionID(sessionID);
        }

        //Setting up the toolbar
        toolbar = findViewById(R.id.topBar);
        setSupportActionBar(toolbar);

        //Setting up the drawer layout
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerView = findViewById(R.id.nav_view);
        setupDrawerContent(drawerView);

        //Enabling the drawer to actually show it's contents
        drawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close);

        // Setup toggle to display hamburger icon with nice animation
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        // Tie DrawerLayout events to the ActionBarToggle
        drawerLayout.addDrawerListener(drawerToggle);

        //Creating the adapter to show giveaway data
        adapter = new MainAdapter(this,giveaway ->
                GiveawayDetailsActivity.startGiveawayDetailsActivity(this,giveaway));

        lastPage = 0;
        pageCount = 1;

        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        //Add line divisor between each element
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));

        recyclerView.setAdapter(adapter);

        refreshLayout = findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this::loadData);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Loading list information
        loadData(pageCount);
    }

    //Setup the listener to the drawer
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
                });
    }

    //What to do when an option is selected in the drawer
    public void selectDrawerItem(MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.login_item_menu:
                Intent intentLogin = new Intent(this, LoginActivity.class);
                startActivity(intentLogin);
                break;

            case R.id.entered_item_menu:
                Intent intentEntered = new Intent(this, EnteredGiveawayActivity.class);
                startActivity(intentEntered);
                break;

            case R.id.profile_item_menu:
                Intent intentProfile = new Intent(this, ProfileActivity.class);
                startActivity(intentProfile);
                break;

            case R.id.blacklist_item_menu:
                Intent intentBlackList = new Intent(this, BlacklistActivity.class);
                startActivity(intentBlackList);
                break;

            case R.id.logout_item_menu:
                //Clearing the user and deleting informations
                currentUser.clear();
                session.setString("sessionID", "");

                loadDrawerData();

                break;
        }

        drawerLayout.closeDrawers();
    }

    //Called by invalidateOptionsMenu, here we decide how to rearrange and redraw the options in the drawer
    //If we place these statements in another place it won't work
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(currentUser.getSessionID() != null){
            drawerView.getMenu().findItem(R.id.logout_item_menu).setVisible(true);
            drawerView.getMenu().findItem(R.id.login_item_menu).setVisible(false);
            drawerView.getMenu().findItem(R.id.blacklist_item_menu).setVisible(true);
            drawerView.getMenu().findItem(R.id.profile_item_menu).setVisible(true);
            drawerView.getMenu().findItem(R.id.entered_item_menu).setVisible(true);
        }else{
            drawerView.getMenu().findItem(R.id.logout_item_menu).setVisible(false);
            drawerView.getMenu().findItem(R.id.login_item_menu).setVisible(true);
            drawerView.getMenu().findItem(R.id.blacklist_item_menu).setVisible(false);
            drawerView.getMenu().findItem(R.id.profile_item_menu).setVisible(false);
            drawerView.getMenu().findItem(R.id.entered_item_menu).setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    //If the drawer icon is selected and we are in the main page we want to show the drawer
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public int getPageCount(){return pageCount;}

    public void loadData(){loadData(1);}

    //load new data from the webpage
    public void loadData(int page) {
        refreshLayout.setRefreshing(true);
        // Here we load more Giveaways
        // At the end of each data list we always append a null giveaway to correctly display
        // a loading bar at the end of the listview
        HTTPClient.INSTANCE.loadGiveaways(page, new GiveawayListCallback() {
            @Override
            public void onSuccess(List<Giveaway> giveawayList,int last) {
                runOnUiThread(() -> {

                    lastPage = last;
                    loadDrawerData();

                    if (!loadedSession && currentUser.getUsername() != null) loadedSession = true;

                    refreshLayout.setRefreshing(false);

                    //when called by onCreate loads the first page only and reset the pagecount
                    if(page == 1){
                        pageCount=1;
                        currentGiveAwayList = giveawayList;
                    }else{
                        //pagecount is > 1 so append the old list with the next page and increment
                        //pagecount
                        pageCount++;

                        // Null item removal
                        currentGiveAwayList.remove(currentGiveAwayList.size() - 1);
                        //Append the new list to the older one
                        currentGiveAwayList.addAll(giveawayList);
                    }
                    if(pageCount < lastPage) currentGiveAwayList.add(null);

                    //submit new currentList to the adapter and notify the change
                    adapter.submitList(currentGiveAwayList);
                    adapter.notifyDataSetChanged();
                });
            }
            @Override
            public void onError() {
                runOnUiThread(() -> {
                    refreshLayout.setRefreshing(false);
                    showErrorSnackbar();
                });
            }
        });
    }

    private void loadDrawerData(){

        userImage = findViewById(R.id.nav_header_imageView);
        userNameTextView = findViewById(R.id.nav_header_username);
        levelTextView = findViewById(R.id.nav_header_level);
        pointsTextView = findViewById(R.id.nav_header_points);

        if(currentUser.getSessionID() != null && !currentUser.getSessionID().isEmpty()){

            userNameTextView.setText(currentUser.getUsername());
            pointsTextView.setText("Punti: " + currentUser.getPoints());
            levelTextView.setText("Livello: " + currentUser.getLevel());

            Glide.with(this)
                    .load(currentUser.getAvatarURL())
                    .apply(RequestOptions.circleCropTransform())
                    .into(userImage);

            levelTextView.setVisibility(View.VISIBLE);
            pointsTextView.setVisibility(View.VISIBLE);

        }else{

            userNameTextView.setText("Effettua il login");
            Glide.with(this)
                    .load(R.drawable.ic_account_circle_24px)
                    .apply(RequestOptions.circleCropTransform())
                    .into(userImage);
            levelTextView.setVisibility(View.INVISIBLE);
            pointsTextView.setVisibility(View.INVISIBLE);
        }

        //invalidate the options in the menu to correctly display available options
        invalidateOptionsMenu();
    }

    private void showErrorSnackbar() {
        View rootView = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView,
                "Errore durante il caricamento dei giveaway", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Riprova", v -> {
            loadData(pageCount + 1);
            snackbar.dismiss();
        });
        snackbar.show();
    }
}
