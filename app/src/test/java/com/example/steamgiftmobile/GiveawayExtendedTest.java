package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.Comment;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

//TODO: test dei commenti
public class GiveawayExtendedTest {

    private GiveawayExtended giveawayExtended;
    private final String id = "id";
    private final String title = "titolo";
    private final GenericUser creator = new GenericUser("creatore");
    private final long timestamp = 300;
    private final String url = "URL";
    private final String description = "description";
    private final int price = 5;
    private final String avatarImageURL = "URL avatar";
    private ArrayList<Comment> comments;
    private int level = 3;
    private boolean joined = false;

    @Before
    public void createGiveawayExtended(){

        comments = new ArrayList<Comment>();

        giveawayExtended = new GiveawayExtended(id,
                title,
                creator,
                timestamp,
                url,
                description,
                comments,
                price,
                level,
                joined);
    }

    @Test
    public void getTest(){

        assertEquals(giveawayExtended.getId(), id);
        assertEquals(giveawayExtended.getTitle(), title);
        assertEquals(giveawayExtended.getCreator(), creator);
        assertEquals(giveawayExtended.getTimestamp(), timestamp);
        assertEquals(giveawayExtended.getThumbnailImageURL(), url);
        assertEquals(giveawayExtended.getDescription(), description);
        assertEquals(giveawayExtended.getPrice(), price);
        assertEquals(giveawayExtended.getLevel(),level);
        assertEquals(giveawayExtended.getJoined(), joined);
    }

    @Test
    public void constructorTest(){

        Giveaway giveaway = new Giveaway(giveawayExtended.getId(),
                giveawayExtended.getTitle(),
                giveawayExtended.getCreator(),
                giveawayExtended.getTimestamp(),
                giveawayExtended.getThumbnailImageURL());

        GiveawayExtended giveawayExtendedDerived = new GiveawayExtended(giveaway,
                description,
                comments,
                price,
                level,
                joined);

        assertEquals(giveaway.getId(), giveawayExtendedDerived.getId());
        assertEquals(giveaway.getTitle(), giveawayExtendedDerived.getTitle());
        assertEquals(giveaway.getCreator(), giveawayExtendedDerived.getCreator());
        assertEquals(giveaway.getTimestamp(), giveawayExtendedDerived.getTimestamp());
        assertEquals(giveaway.getThumbnailImageURL(),giveawayExtendedDerived.getThumbnailImageURL());
    }

    @Test
    public void exceptionTest(){

        assertThrows(IllegalArgumentException.class, ()->{
            giveawayExtended = new GiveawayExtended(id,
                    title,
                    creator,
                    timestamp,
                    url,
                    description,
                    comments,
                    -1,
                    level,
                    joined);});

        assertThrows(IllegalArgumentException.class, ()->{
            giveawayExtended = new GiveawayExtended(id,
                    title,
                    creator,
                    timestamp,
                    url,
                    description,
                    comments,
                    -13,
                    level,
                    joined);});

        assertThrows(IllegalArgumentException.class, ()->{
            giveawayExtended = new GiveawayExtended(id,
                    title,
                    creator,
                    timestamp,
                    url,
                    description,
                    null,
                    price,
                    level,
                    joined);});

        assertThrows(IllegalArgumentException.class, ()->{
            giveawayExtended = new GiveawayExtended(id,
                    title,
                    creator,
                    timestamp,
                    url,
                    description,
                    comments,
                    price,
                    -1,
                    joined);});
    }
}
